package com.company;
import java.util.Scanner;
import java.util.Random;
import java.util.Collections;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        // write your code here
        // using Random class we will specify the number to be guessed
        Random random_num = new Random();
        int name ;

        //Specifying Scanner class in order to be able to take an input from the user
        Scanner reader = new Scanner (System.in);
        //As we are asked to check whether input is a number
        //I will take input as a string and then check whether it is number or not
        String guess;

        int count=0;

        //In question it is forbidden to use list
        // (i did not know if we can use Arraylist in order to be able to resize)
        //So i used simple array to store the guessed numbers
        Integer[] guessed_numbers = new Integer[100001];

        //For purpose of sorting in reverse order in further steps ,
        // i fill the array with zeros , so we would not get an error:
        Arrays.fill(guessed_numbers,0);


        //Processing the whole game in the infinite cycle
        while (true){

            //The text "Let the game begin!" appears in the screen before the game begins
            //I used asteriks in order to better visualize during input when new game begins
            System.out.println("*********************");
            System.out.println("Let the game begin!");


            // Specifying the random number to be guessed
            //It is in range 0-100
            name = random_num.nextInt(101);

            //I will display the generated number because sometimes it is very difficult to guess
            // (You can comment not to see)
            System.out.println("The randomly generated number: " + name );

            //Guessing the randomly generated number
            System.out.println("Please enter the hidden number (it is in range 0-100):");
            guess=reader.nextLine();


            //Flag is initially false
            boolean flag = false ;

            //CHECKING CORRECTNESS OF DATA
            //We enter a while loop and check input until it is number
            while (flag!=true){
                for (int i = 0; i < guess.length(); i++){
                    if (!Character.isDigit(guess.charAt(i))){
                        System.out.println("!!!!!!! Your input is not a number  ");
                        //If input is not a number , we inform the user and ask for input again
                        System.out.println("Please enter the hidden variable  (it is in range 0-100):");
                        guess=reader.nextLine();
                        break;
                    }
                    // If the program did not break and we reach this part
                    //It means input was a number
                    flag=true;
                }
            }
            //We change the entered input string into integer for further use
            int guess2=Integer.parseInt(guess);


            //Each time I will add the number entered by the user as a guess to the array
            guessed_numbers[count]=guess2;
            //I will use count to specify in the for loop
            // for printing the array elements
            count++;


            //I will use the if condition in order
            // to compare the generated random number and number entered by user
            if (guess2 < name ) System.out.println("Your number is too small. Please, try again.");
            else if (guess2>name) System.out.println("Your number is too big. Please, try again.");
            else {
                System.out.println("Congratulations, " + name + "! ");


                //If the number is guessed correctly
                //We first of all sort the entered numbers in descending order
                Arrays.sort(guessed_numbers,Collections.reverseOrder());

                //And now print them
                System.out.println("Your numbers : ");
                for (int i=0;i<count;i++){
                    System.out.print(guessed_numbers[i]+ " ");
                }
                System.out.println();

            }
        }
    }
}
